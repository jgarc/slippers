# Slippers
This repo has been built and maintained using macOS with some consideration for Windows machines. Linux support has not been tested. Feel free to open an issue if you have any questions. 

## Local Development Instructions
1. If you don't already, make sure you have [yarn](https://yarnpkg.com/getting-started/install), [node](https://nodejs.org), and [nvm](https://github.com/nvm-sh/nvm) installed  (For windows, you will need to install [nvm-windows](https://github.com/coreybutler/nvm-windows), and [git](https://git-scm.com/)).
1. `nvm install 15.4.0`
1. `nvm use 15.4.0`
1. `yarn` to install `storybook`, `tailwindcss`, and their dependencies
1. `yarn watch:css` to watch for changes within `html` files within `./stories` directory.
1. `yarn storybook` to run on port 6006
1. `yarn build-storybook` to build to  `/storybook-static` directory 


# Storybook
## Notes
* Storybook currently must use PostCSSv7, while Tailwindv2 is using PostCSSv8. Because of this, Tailwind was installed to be compatible with PostCSSv7. [Tailwind Docs on compatibility](https://tailwindcss.com/docs/installation#post-css-7-compatibility-build).
* This contains an HTML community-maintained edition of Storybook. Because of that, much of [Storybook](https://storybook.js.org/docs/html/get-started/introduction) is built and written with JS frameworks/Web Components in mind.


# TailwindCSS
## Notes
* Under the hood, TailwindCSS uses [modern-normalize](https://github.com/sindresorhus/modern-normalize) as CSS reset across browsers. [Tailwind on Preflight](https://tailwindcss.com/docs/preflight).
* TailwindCSS was installed as a PostCSS Plugin. [Tailwind Installation Instructions](https://tailwindcss.com/docs/installation#add-tailwind-as-a-post-css-plugin).
* Tailwind config lives within `tailwind.config.js`, were
* Recommended VSCode Extension: [Tailwind CSS IntelliSense](https://marketplace.visualstudio.com/items?itemName=bradlc.vscode-tailwindcss)
* Building CSS without using CSSPurge makes CSS file 3.9MB in size.

# PostCSS
## Notes
* [Autoprefixer](https://autoprefixer.github.io/): PostCSS Plugin adds ventor browser prefixes
* [PostCSS Import]():

<!-- File will be minified + optimized with autoprefixer -->