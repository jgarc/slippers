module.exports = {
  purge: {
    enabled: true,
    content: [
      './stories/**/*.html'
    ],
    options: {
      safelist: [''],
    }
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      white: '#FFF',
      black: '#333',
      brand: {
        yellow: '#FCA121',
        orange: '#FC6D26',
        red: '#DB3B21',
        turquoise: '#52CDB7',
        purple: '#9B51E0'
      },
      grayscale: {
        DEFAULT: '#999',
        gray2: '#5E5E5E',
        gray3: '#404040'
      },
      ui: {
        primary: {
          DEFAULT: '#FA7035',
          light: '#FF8D5D',
          dark: '#DD6531'
        },
        secondary: {
          DEFAULT: '#9B51E0',
          light: '#B37DE6',
          dark: '#7D2BD9'
        },
        tertiary: {
          DEFAULT: '#52CDB7',
          light: '#9EEBDD',
          dark: '#1DBA9E'
        }
      },
      status: {
        success: '#48A651',
        warning: '#FFA561',
        error: '#F24F63',
        info: '#A2CEFF'
      }
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
