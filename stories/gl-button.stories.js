import { withLinks } from '@storybook/addon-links';
import { withDesign } from 'storybook-addon-designs';

import GlButton from './gl-button.html';

export default {
    title: 'Buttons',
    decorators: [withLinks, withDesign],
};

export const PrimaryButton = () => GlButton;
console.dir(typeof GlButton)

PrimaryButton.parameters = {
    design: {
        type: 'figma',
        url: 'https://www.figma.com/file/nWIOpmuMp7RZXmfTj6ujAF/Slippers_foundations?node-id=113%3A3',
    },
}
